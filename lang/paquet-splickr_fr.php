<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
$GLOBALS[$GLOBALS['idx_lang']] = array(
	// S
	'splickr_description' => 'Noisette pour afficher les dernieres photos à la Flickr (le "badge"), tout ça en javascript via <code>#INSERT_HEAD</code> et JQuery.

-* On peut l\'utiliser en modele avec <code><splickrbox></code> en lui passant en option un parametre d\'id pour limiter la boucle document,
-* On peut l\'utiliser dans les squelettes avec la balise <code>#MODELE{splickrbox}</code>
-* On peut l\'utiliser avec différent paramètres (qu\'on peut aussi combiner):
-** <code><splickrbox|right></code> et <code><splickrbox|left></code> pour les alignements
-** <code><splickrbox|taille=24></code> permet de spécifier le nombre de vignettes à afficher. Ce nombre devrait être un multiple du nombre de colonnes.
-** <code><splickrbox|colonnes=10></code> permet de spécifier le nombre de colonnes à afficher.
-** <code><splickrbox|masque=carre-100.png></code> permet de définir la taille des vignettes (le masque correspond à la grande vignette).
-** <code><splickrbox|tri=date></code> permet de spécifier le classement (date, hasard...).
-** <code><splickrbox|senstri=1></code> permet d\'inverser le sens de classement choisi ci-dessus.',
	'splickr_slogan' => 'Afficher des photos à la Flickr',
);
