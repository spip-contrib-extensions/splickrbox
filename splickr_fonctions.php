<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function splickr_juste_a_x($compteur, $x, $return) {
	if ($compteur % $x == 0) {
		return $return;
	}

	return '';
}

function splickr_insert_head($flux) {
	$flux .= '<script type="text/javascript" src="' . timestamp(find_in_path('splickrbox.js')) . '"></script>';

	return $flux;
}
